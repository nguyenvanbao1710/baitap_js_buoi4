var doanHinhTamGiac = function () {
  var canhThuNhatValue = document.getElementById("edge1").value * 1;
  var canhThuHaiValue = document.getElementById("edge2").value * 1;
  var canhThuBaValue = document.getElementById("edge3").value * 1;
  var loaiTamGiac = null;

  if (
    canhThuNhatValue == canhThuHaiValue &&
    canhThuHaiValue == canhThuBaValue
  ) {
    loaiTamGiac = "Đây là tam giác đều";
  } else if (
    canhThuNhatValue == canhThuHaiValue ||
    canhThuNhatValue == canhThuBaValue ||
    canhThuHaiValue == canhThuBaValue
  ) {
    loaiTamGiac = "Đây là tam giác cân";
  } else if (
    Math.pow(canhThuNhatValue, 2) ==
      Math.pow(canhThuHaiValue, 2) + Math.pow(canhThuBaValue, 2) ||
    Math.pow(canhThuHaiValue, 2) ==
      Math.pow(canhThuNhatValue, 2) + Math.pow(canhThuBaValue, 2) ||
    Math.pow(canhThuBaValue, 2) ==
      Math.pow(canhThuNhatValue, 2) + Math.pow(canhThuHaiValue, 2)
  ) {
    loaiTamGiac = "Đây là tam giác vuông";
  } else {
    loaiTamGiac = "Loại tam giác khác";
  }

  document.getElementById("result2").innerHTML = loaiTamGiac;
};
